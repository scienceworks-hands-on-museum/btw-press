from .Component import Component
from eekquery.DataObject import DataObject
import json
import subprocess

class Components:

    _DATA_PATH = '.components.json'

    def _getComponents(self):
        try:
            with open(Components._DATA_PATH, 'r') as f:
                return json.loads(f.read())
        except FileNotFoundError:
            return {}

    def _writeComponents(self, components):
        with open(Components._DATA_PATH, 'w') as f:
            f.write(json.dumps(components))

    def get(self, name):
        components = self._getComponents()
        componentsList = components.get('components', [])

        for component in componentsList:
            if name == component['name']:
                component = Component(component)

                component.readCodeFromFiles()

                return component.asDict()

        return None

    def list(self):
        '''
        List all available components.
        '''

        return self._getComponents().get('components', [])

    def write(self, component):
        create = component.get('title', None) is None

        component = Component(component)

        if create:
            subprocess.run('src/JSUtils/scripts/create_component.sh {}'.format(component.name).split())

        components = self._getComponents()
        componentsList = components.get('components', [])
        found = False

        for i in range(len(componentsList)):
            c = componentsList[i]

            if c['path'] is component.path:
                c[i] = component
                found = True

                break

        if not found:
            componentsList.append(component.asDict())

        components['components'] = componentsList
        self._writeComponents(components)

    def delete(self):
        pass