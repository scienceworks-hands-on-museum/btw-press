from btwpress.TitleFactory import TitleFactory
from eekquery.DataObject import DataObject
import os

class Component(DataObject):

    TABLE = 'Components'

    def __init__(self, fromDict):

        super().__init__(fromDict)

        self.addField('title')
        self.addField('name')
        self.addField('path')
        self.addField('html')
        self.addField('css')
        self.addField('js')

        if not self.hasPropertyNotNone('title'):
            self.title = TitleFactory.produce()

        if not self.hasPropertyNotNone('name'):
            self.name = self.title.replace(' ', '') + 'Component'

        if not self.hasPropertyNotNone('path'):
            self.path = 'src/components/' + self.name

    def readCodeFromFiles(self):
        htmlPath = os.path.join(self.path, '_index.html')
        with open(htmlPath, 'r') as f:
            self.html = f.read()

        cssPath = os.path.join(self.path, 'style.css')
        with open(cssPath, 'r') as f:
            self.css = f.read()

        jsPath = os.path.join(self.path, 'driver.js')
        with open(jsPath, 'r') as f:
            self.js = f.read()