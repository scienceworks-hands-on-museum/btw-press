from .Webpage import Webpage
from btwpress.Website import Website
from eekquery.DataObject import DataObject
import json
import subprocess

class Webpages:

    _DATA_PATH = '.website-data.json'

    def _getWebsite(self):
        try:
            with open(Webpages._DATA_PATH, 'r') as f:
                return Website(json.loads(f.read()))
        except FileNotFoundError:
            return Website({})

    def _writeWebsite(self, site):
        with open(Webpages._DATA_PATH, 'w') as f:
            f.write(json.dumps(site, cls=DataObject.JSONEncoder))

    def listPages(self):
        '''
        List all Webpages in the site.
        '''

        website = self._getWebsite()

        return map(lambda x: Webpage(x).asDict(), website.pages)

    def addPage(self):
        '''
        Add a Webpage to the site.
        '''

        site = self._getWebsite()

        page = Website({})

        site.pages.append(page)

        subprocess.run('src/JSUtils/scripts/create_view.sh {} {}'.format(page.pathname, page.title).split())

        self._writeWebsite(site)
