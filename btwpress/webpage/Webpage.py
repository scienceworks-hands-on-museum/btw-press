from btwpress.TitleFactory import TitleFactory
from eekquery.DataObject import DataObject

class Webpage(DataObject):

    TABLE = 'Webpages'

    def __init__(self, fromDict):

        super().__init__(fromDict)

        self.addField('title')
        self.addField('description')
        self.addField('image')
        self.addField('pathname')
        self.addField('content')

        if not self.hasPropertyNotNone('title'):
            self.title = TitleFactory.produce()

        if not self.hasPropertyNotNone('pathname'):
            self.pathname = '/' + self.title.lower().replace(' ', '-')