import random

class TitleFactory():

    _ADJECTIVES_PATH = 'adjectives.txt'
    _ANIMAL_NAMES_PATH = 'animal-names.txt'

    @classmethod
    def produce(cls):
        with open(cls._ADJECTIVES_PATH, 'r') as f0:
            with open(cls._ANIMAL_NAMES_PATH, 'r') as f1:
                a = random.choice(f0.readlines()).strip()
                b = random.choice(f1.readlines()).strip()

                return a[0].upper() + a[1:] + ' ' + b[0].upper() + b[1:]
