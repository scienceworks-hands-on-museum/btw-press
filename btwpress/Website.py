from eekquery.DataObject import DataObject

class Website(DataObject):

    TABLE = 'Websites'

    def __init__(self, fromDict):

        super().__init__(fromDict)

        self.addField('pages')

        if not self.hasPropertyNotNone('pages'):
            self.pages = []

