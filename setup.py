#!/usr/bin/env python3

from os import environ
from setuptools import setup, find_packages
from setuptools.command.install import install
from subprocess import check_call as execute

repoDependencies = [
    './PythonLambdaServer',
    './authentication',
    './src/JSUtils/Slurp',
]

class PostInstallCommand(install):
    def run(self):
        for repo in repoDependencies:
            target = environ.get('INSTALL_TARGET', None)
            if isinstance(repo, tuple):
                repo = '{}@{}'.format(*repo)

            if target is not None:
                print('pip install {} --target={}'.format(repo, target))
                execute('pip install --upgrade {} --target={}'.format(repo, target), shell=True)
            else:
                execute('pip install --upgrade {}'.format(repo).split())

        # execute('cd ./EekQuery && make install')

        install.run(self)

setup(name='btw-press',
    version='1.0',
    description='',
    author='John D. Sutton',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'nose',
        'nose-exclude',
        'coverage',
        'sphinx',
    ],
    cmdclass={
        'install': PostInstallCommand
    }
)
