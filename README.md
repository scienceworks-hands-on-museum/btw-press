# Fullstack Project Template
Uses a Python 3 backend and a JSUtils frontend framework.

## Getting Started

### Installation
```
git clone git@gitlab.com:jdsutton/pythonproject.git
cd pythonproject

sudo apt update
sudo apt install virtualenv build-essential

# Make sure python3.6 is installed on your system:
python3 --version

virtualenv -p python3.6 env
. env/bin/activate

make install
```

### Run the webserver
```
# Make sure you have done . env/bin/activate first

make
```

Now you may open the page at http://0.0.0.0:50000