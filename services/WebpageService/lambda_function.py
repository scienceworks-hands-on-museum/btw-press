from eekquery.ConfigFactory import ConfigFactory
from pythonlambdaserver.LambdaService import LambdaService
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig

class WebpageService(LambdaService):
    '''
    TODO.
    '''

    METHOD_CONFIGURATION = {
        'default': {
            'listPages': LambdaServiceMethodConfig(True),
            'addPage': LambdaServiceMethodConfig(True),
            'deletePage': LambdaServiceMethodConfig(True),
            'writePage': LambdaServiceMethodConfig(True),
        }
    }

    @property
    def default(self):
        from btwpress.webpage.Webpages import Webpages

        config = ConfigFactory.produce('config/btwpress.config.json')

        return Webpages()

def lambda_handler(event, context):
    return WebpageService.handleEvent(event, context)