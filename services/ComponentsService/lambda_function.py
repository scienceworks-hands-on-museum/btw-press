from pythonlambdaserver.LambdaService import LambdaService
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig

class ComponentsService(LambdaService):
    '''
    TODO.
    '''

    METHOD_CONFIGURATION = {
        'default': {
            'list': LambdaServiceMethodConfig(True),
            'get': LambdaServiceMethodConfig(True),
            'write': LambdaServiceMethodConfig(True),
            'delete': LambdaServiceMethodConfig(True),
        }
    }

    @property
    def default(self):
        from btwpress.components.Components import Components

        return Components()

def lambda_handler(event, context):
    return ComponentsService.handleEvent(event, context)