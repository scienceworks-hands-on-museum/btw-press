import('/src/components/Components.js');
import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');

/**
 * @class
 */
class AdminComponentEditViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        const componentName = QueryString.getAsObj().name;

        const tabManager = new ToggleableTabManagerComponent(
            ['HTML', 'CSS', 'JS'],
            [ID.HTML_CONTAINER, ID.CSS_CONTAINER, ID.JS_CONTAINER],
        );

        Dom.setContents(ID.TAB_MANAGER, tabManager.render());

        Components.get(componentName, {}).then((component) => {
            this._initEditor(component);
        });
    }

    /**
     * @private
     */
    static _initEditor(component) {
        let editor;

        Dom.setContents(ID.HTML_CONTAINER, component.html);
        editor = ace.edit(ID.HTML_CONTAINER);
        editor.setTheme('ace/theme/monokai');
        editor.session.setMode('ace/mode/html');

        Dom.setContents(ID.CSS_CONTAINER, component.css);
        editor = ace.edit(ID.CSS_CONTAINER);
        editor.setTheme('ace/theme/monokai');
        editor.session.setMode('ace/mode/css');

        Dom.setContents(ID.JS_CONTAINER, component.js);
        editor = ace.edit(ID.JS_CONTAINER);
        editor.setTheme('ace/theme/monokai');
        editor.session.setMode('ace/mode/javascript');
    }
}

Page.addLoadEvent(() => {
    AdminComponentEditViewDriver.init();
});

const ID = {
    HTML_CONTAINER: 'htmlContainer',
    CSS_CONTAINER: 'cssContainer',
    JS_CONTAINER: 'jsContainer',
    TAB_MANAGER: 'tabManager',
};

const CLASS = {

};
