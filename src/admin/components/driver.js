import('/src/components/Components.js');
import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');

/**
 * @class
 */
class AdminComponentnsViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        this._listComponents();
    }

    /**
     * @private
     */
    static _listComponents() {
        Components.list().then((components) => {
            if (!components.length) {
                Dom.setContents(ID.CONTAINER, 'No components yet.');
            } else {
                Dom.setContents(ID.CONTAINER, '');
            }

            components.forEach((component) => {

                Dom.appendTo(ID.CONTAINER, new ComponentListItemComponent(component).render());
            });
        });
    }

    /**
     * @public
     */
    static addComponent() {
        Components.write({}, {}).then(() => {
            this._listComponents();
        });
    }
}

Page.addLoadEvent(() => {
    AdminComponentnsViewDriver.init();
});

const ID = {
    CONTAINER: 'container',
};

const CLASS = {

};
