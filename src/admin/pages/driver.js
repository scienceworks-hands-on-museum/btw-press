import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('/src/webpage/Webpages.js');

/**
 * @class
 */
class PagesViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        this._listPages();
    }

    /**
     * @private
     */
    static _listPages() {
        Webpages.listPages().then((pages) => {
            if (!pages.length) {
                Dom.setContents(ID.CONTAINER, 'No pages yet.');
            } else {
                Dom.setContents(ID.CONTAINER, '');
            }

            pages.forEach((page) => {
                Dom.appendTo(ID.CONTAINER, `<li><a href="${page.pathname}">${page.pathname}: ${page.title}</a></li>`);
            });
        });
    }

    /**
     * @public
     */
    static addPage() {
        Webpages.addPage().then(() => {
            this._listPages();
        })
    }
}

Page.addLoadEvent(() => {
    PagesViewDriver.init();
});

const ID = {
    CONTAINER: 'container',
};

const CLASS = {

};
