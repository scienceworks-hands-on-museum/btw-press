import('/src/JSUtils/util/DataObject.js');

/**
 * @class
 */
class Webpage extends DataObject {
    
    /**
     * @constructor
     */
    constructor(obj) {
        super(obj, false, true);
    }
    
    /**
     * @public
     */
    init() {

    }

    /**
     * @public
     */
    static fromFormData(data) {
        let result = new Webpage(data);


        return result;
    }

    /**
     * @public
     */
    toFormData() {
        let result = Object.assign({}, this);


        return result;
    }
}