import('/src/JSUtils/util/Dom.js');

/**
 * @public
 */
class Appbar {

    /**
     * @public
     */
    static init() {
        if (!window.location.pathname.startsWith('/admin')) {
            Dom.getById(Appbar.ID.EDIT_MODE).style.display = 'flex';
        }
    }

    /**
     * @public
     */
    static toggleEditMode() {
        const editMode = Dom.getDataFromForm(Appbar.ID.APPBAR).editMode;

        if (editMode) {
            let container = Dom.getById(Appbar.ID.CONTAINER);
            const button = new AddContentComponent();

            Dom.prependTo(container, button.render());
            Dom.appendTo(container, button.render());
        } else {
            for (let c of Dom.getByClass('AddContentComponent')) {
                Dom.removeElement(c);
            }
        }
    }
}

Appbar.ID = {
    APPBAR: 'appbar',
    CONTAINER: 'container',
    EDIT_MODE: 'appbar-edit-mode',
};

Appbar._editing = false;

Appbar.init();
