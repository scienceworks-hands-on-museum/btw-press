import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class AddContentComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        const params = {};
        
        super(AddContentComponent.ID.TEMPLATE.THIS, params);
    }

    /**
     * @public
     */
    addContent() {
        const component = new NewContentFormComponent();

        Dom.appendTo(document.body, component.render());
    }
}

AddContentComponent.ID = {
    TEMPLATE: {
        THIS: 'AddContentComponent_template',
    },
};
