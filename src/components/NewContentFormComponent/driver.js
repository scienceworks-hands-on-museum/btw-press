import('/src/JSUtils/util/Select.js');

/**
 * @class
 */
class NewContentFormComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor() {

        const listF = (query) => {
            return Promise.resolve(NewContentFormComponent.CONTENT_TYPES);
        };

        const renderF = (item) => {
            return `<div>${item}</div>`;
        };

        const params = {};
        
        super(NewContentFormComponent.ID.TEMPLATE.THIS, params);

        this._params.search = new SearchSuggestionComponent(
            null,
            {},
            listF,
            renderF,
            this.getId() + this.constructor.ID.SEARCH_RESULTS,
        ).render();
    }
}

NewContentFormComponent.ID = {
    SEARCH_RESULTS: '_searchResults',
    TEMPLATE: {
        THIS: 'NewContentFormComponent_template',
    },
};

NewContentFormComponent.CONTENT_TYPES = [
    'Heading 1',
    'Heading 2',
    'Heading 3',
    'Heading 4',
    'Heading 5',
    'Heading 6',
    'Paragraph',
    'Ordered List',
    'Unordered List',
    'Image',
    'Video',
];
