import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class ComponentListItemComponent extends Component {

    /**
     * @constructor
     */
    constructor(component) {
        const params = Object.assign({}, component);
        
        super(ComponentListItemComponent.ID.TEMPLATE.THIS, params);
    }
}

ComponentListItemComponent.ID = {
    TEMPLATE: {
        THIS: 'ComponentListItemComponent_template',
    },
};
