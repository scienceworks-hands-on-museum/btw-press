import('/src/JSUtils/util/DataObject.js');

/**
 * @class
 */
class ComponentDataObject extends DataObject {
    
    /**
     * @constructor
     */
    constructor(obj) {
        super(obj, false, true);
    }
    
    /**
     * @public
     */
    init() {

    }

    /**
     * @public
     */
    static fromFormData(data) {
        let result = new ComponentDataObject(data);


        return result;
    }

    /**
     * @public
     */
    toFormData() {
        let result = Object.assign({}, this);


        return result;
    }
}